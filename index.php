<?php
    date_default_timezone_set("America/New_York");
	$title = " | Dashboard";
    require("./includes/views/header.php"); 
?> 

<?php  
	
	// Connect to the Database
	$db = pg_connect("dbname=cyndy08 user=cyndy08 password=123456")or die ("Failed to connect to DB: " . pg_last_error()); 
	 
	// Query database
	$author = pg_query('SELECT COUNT(*) FROM authors;');  
	$editor = pg_query("SELECT COUNT(*) FROM editor;"); 
	
	$article = pg_query("SELECT COUNT(*) FROM article;");
	$book = pg_query("SELECT COUNT(*) FROM book;");
	$incollection = pg_query("SELECT COUNT(*) FROM incollection;");
	$inproceedings = pg_query("SELECT COUNT(*) FROM inproceedings;");
	$www = pg_query("SELECT COUNT(*) FROM www;");
	$phdthesis = pg_query("SELECT COUNT(*) FROM phdthesis;");
	$masterthesis = pg_query("SELECT COUNT(*) FROM masterthesis;");
	
	$numAuthor = pg_fetch_row($author);
	$numEditor = pg_fetch_row($editor);
	
	
	$numarticle = pg_fetch_row($article);
	$numbook = pg_fetch_row($book);
	$numincollection = pg_fetch_row($incollection);
	$numinproceedings = pg_fetch_row($inproceedings);
	$numwww = pg_fetch_row($www);
	$numphdthesis = pg_fetch_row($phdthesis);
	$nummasterthesis = pg_fetch_row($masterthesis);
?> 


    <contents>
        <div class="row medium-unstacked" id="startOfContents">
          	<div class="medium-12 columns"> 
                    <div class="sections">
                      <section id="about" data-magellan-target="about">
                      
                      	<div class="large callout">
                        
                        	<h4>About</h4>
                            <hr />
                            <p><i class="fa fa-book fa-3x fa-pull-left light" aria-hidden="true"></i>
<b>BiblioVT - FDC</b> system provides open bibliographic information on about approximately 1.4 million publications in the computer science literature.</p>
                            <h4>Features</h4>
                            <hr />
                            <p>This system displays the data within the database, models the data based on relational schema, responds to searches and other relational queries, and displays relevant data and user information via a unique interface.</p>
                            <p>It also supports:</p>
                            <ul> 
                                <li>Article recommendations</li>  
                            	<li>Visual representations for analyzing the data that we have in the database</li>
                            </ul>
                        </div>
                      	
                      
                      
                      </section>
                      <section id="queries" data-magellan-target="queries">
                      		<div class="large callout">
                          
                            <h4>Queries</h4>
                            <hr /> 
                            <form id="queryReq" action="../~cyndy08/get-results.php" method="POST"> 
                            <h5 class="vtorange"><b>Relations</b></h5>
                            <p>Clicking a button below will execute an SQL query and list 20 tuples in that particular relation.</p> 
                            	<input type="submit" class="success button" value="Article"  onclick="getRelation(1);">
                            	<input type="submit" class="success button" value="Book"  onclick="getRelation(2);">
                            	<input type="submit" class="success button" value="WWW_Write"  onclick="getRelation(3);">
                            	<input type="submit" class="success button" value="Edited_By"  onclick="getRelation(4);">
                            	<input type="submit" class="success button" value="Publication"  onclick="getRelation(5);">
        
                            <h5 class="vtorange"><b>SQL Queries</b></h5>
                            <p><i class="fa fa-database fa-3x fa-pull-left light" aria-hidden="true"></i>Select from default queries or enter an ad-hoc query below, for example: <code>SELECT * FROM www LIMIT 1;</code>. See <a href="#faqs"><b>FAQs</b></a> for more information on how to use this feature.</p>
                            
                              <div class="row">
                                <div class="medium-6 columns">
                                  <label><b>Ad-Hoc Query</b>
                                    <div class="input-group"> 
                                      <input class="input-group-field" type="text" id="ad-hoc" name="ad-hoc">
                                      <div class="input-group-button">
                                        <input type="submit" class="button" value="Submit" onclick="adhocQ();" class="adhocSubmit">
                                      </div>
                                    </div>
                                  </label>
                                </div>
                                <div class="medium-6 columns">
                                <label><b>Default Queries</b>
                                <div class="input-group"> 
                                  <select  class="input-group-field" id="defQuery" name="defQuery">
                                        <option value="default">Select</option>
                                        <option disabled>----------------------------------------</option>
                                        <option value="1">Publications: by Philip S. Yu</option>
                                        <option value="2">Publications: from conference to a journal</option>
                                        <option value="3">Scientists with the highest # of collaborators</option>  
                                  </select>
                                  <div class="input-group-button">
                                    <input type="submit" class="button" value="Submit" onclick="getQuery();">
                                  </div>
                                </div> 
                                 </label>
                                </div>
                              </div>
                              <input type="hidden" id="query" name="query"/> 
                            <h5 class="vtorange"><b>SQL Results</b></h5>
                            <p><small>Your query results will appear in a separate page.</small></p>  
                            
                        	</div>
                      
                      
                      </section>
                      <section id="tools" data-magellan-target="tools">
                      		<div class="large callout">
                          
                            <h4>Tools</h4>
                            <hr /> 
                            <ul class="tabs" data-tabs id="example-tabs">
                              <li class="tabs-title is-active"><a href="#panel1" aria-selected="true"><h6 class="vtorange"><b>Recommendations</b></h6></a></li>
                              <li class="tabs-title"><a href="#panel2"><h6 class="vtorange"><b>Data Visualization</b></h6></a></li> 
                            </ul>
                           <div class="tabs-content" data-tabs-content="example-tabs">
                              <div class="tabs-panel is-active" id="panel1">
                                
                                 
                                  <div class="row">
                                    <div class="medium-6 columns">
                                     Enter a keyword in the search box on the right to view a list of (max 20) articles related to the keyword you entered. For example <code>graph</code>. 
                                    </div>
                                    <div class="medium-6 columns"> 
                                        <div class="input-group"> 
                                          <input class="input-group-field" type="text" id="rec" name="rec">
                                          <div class="input-group-button">
                                            <input type="submit" class="button" value="Search" onclick="recommend();">
                                          </div>
                                        </div> 
                                    </div>
                                  </div> 
                                </form>
                                
                             </div>
                              <div class="tabs-panel" id="panel2">
                              	<p>Below are pie graphs that represents the types of publications we have in the database. </p>
                                <div class="row">
                                <div class="medium-6 columns">
                                <div id="chart_div">
                                    <!--Div that will hold the pie chart for gender-->
                                    <i class="fa fa-circle-o-notch fa-spin fa-fw margin-bottom"></i>
                                        <span><small>Loading data...</small></span>
                                </div>
                                </div>
                                
                                <div class="medium-6 columns">
                                 <div id="chart_div2">
                                    <!--Div that will hold the pie chart for gender-->
                                    <i class="fa fa-circle-o-notch fa-spin fa-fw margin-bottom"></i>
                                        <span><small>Loading data...</small></span>
                                </div>
                                </div>
                              </div>
                            </div>
                        	</div>
                      
                      
                      </section>
                      
                      <section id="faqs" data-magellan-target="faqs">
                      		<div class="large callout">
                          
                            <h4>FAQs</h4>
                            <hr />
                           
                           	<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                              <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title"><h6 class="vtorange"><b>Running your own Query</b></h6></a>
                                <div class="accordion-content" data-tab-content>
                                 <p> Use our Ad-Hoc Query tool to run your own query. Enter your query using the sample format: <code>SELECT * FROM www LIMIT 1;</code>. Please note that we are currently not limiting the capability of this tool and thus we ask you to refrain from running queries that could modify or delete data from our database.  </p>
                                </div>
                              </li> 
                              <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title"><h6 class="vtorange"><b>Article Recommendations</b></h6></a>
                                <div class="accordion-content" data-tab-content>
                                 <p> Use our article recommendations tool to look up articles related to the keyword you entered. Please note that this search tool is case sensitive and therefore searching for <code>ES</code> would return results different from <code>es</code>. We are also only searching for publications of type "article."</p>
                                </div>
                              </li> 
                              <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title"><h6 class="vtorange"><b>Data Visualization</b></h6></a>
                                <div class="accordion-content" data-tab-content>
                                 <p> The pie graphs under visualization tool are great way to get an overview of what type of publications we have stored in our database. Simply hover your mouse on each individual slices to get more information. Click on a slice if you would like to highlight it. </p>
                                </div>
                              </li> 
                            </ul>
                           
                        	</div>
                      
                      
                      </section>
                    </div> 
            </div> 
        </div>
    </contents>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
 
	var author = <?php echo json_encode( $numAuthor );  ?>; 
	var editor = <?php echo json_encode( $numEditor );  ?>; 
	
	var article = <?php echo json_encode( $numarticle );  ?>; 
	var book = <?php echo json_encode( $numbook );  ?>; 
	var incollection = <?php echo json_encode( $numincollection );  ?>; 
	var inproceedings = <?php echo json_encode( $numinproceedings );  ?>; 
	var www = <?php echo json_encode( $numwww );  ?>; 
	var phdthesis = <?php echo json_encode( $numphdthesis );  ?>; 
	var masterthesis = <?php echo json_encode( $nummasterthesis );  ?>; 
	<!-- Google Chart -->
			// Load the Visualization API and the corechart package.  
			  google.charts.load('current', {'packages':['corechart']});
		
			  // Set a callback to run when the Google Visualization API is loaded.
			  google.charts.setOnLoadCallback(drawChart);
		
			  // Callback that creates and populates a data table,
			  // instantiates the pie chart, passes in the data and
			  // draws it.
			  function drawChart() {
			
				// Create the data table.
				var data = new google.visualization.DataTable();
				data.addColumn('string', 'Type');
				data.addColumn('number', 'Ratio');
				data.addRows([
				  ['Author', parseInt(author)],
				  ['Editor', parseInt(editor)],  
				]);
				// 
				// chartArea: {left:20,top:15,width:'90%',height:'90%'}, ,
				// legend: {position: 'top'}
				// Set chart options
				var options = { title:'Number of Authors and Editors',
								width: 440,
								height: 400 };
		
				// Instantiate and draw our chart, passing in some options.
				var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
				chart.draw(data, options);
			  }
			   google.charts.setOnLoadCallback(drawChart2);
			  // draws second pie chart
			  function drawChart2() {
			
				// Create the data table.
				var data = new google.visualization.DataTable();
				data.addColumn('string', 'Type');
				data.addColumn('number', 'Ratio');
				data.addRows([
				  ['article', parseInt(article)],
				  ['book', parseInt(book)], 
				  ['incollection', parseInt(incollection)],  
				  ['inproceedings', parseInt(inproceedings)],  
				  ['www', parseInt(www)],  
				  ['phdthesis', parseInt(phdthesis)],  
				  ['masterthesis', parseInt(masterthesis)] 
				]);
				// 
				// chartArea: {left:20,top:15,width:'90%',height:'90%'}, ,
				// legend: {position: 'top'}
				// Set chart options
				var options = { title:'Types of Publications',
								width: 440,
								height: 400 };
		
				// Instantiate and draw our chart, passing in some options.
				var chart = new google.visualization.PieChart(document.getElementById('chart_div2'));
				chart.draw(data, options);
			  }
</script>
<?php
    require("./includes/views/footer.php");
?>
