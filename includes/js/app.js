$(document).foundation()


function adhocQ() {  
	var input = document.getElementById('ad-hoc'); 
	var data = document.getElementById('query');

	data.value = input.value;
	form.submit(); 
}

function recommend() {  
	var input = document.getElementById('rec'); 
	var data = document.getElementById('query');
	var a = "SELECT * FROM (SELECT * FROM publication p, article_write a WHERE a.dblp_key = p.dblp_key) A WHERE A.title LIKE '";
	var b = "%' LIMIT 20;";
	var c = a.concat(input.value); 
	data.value =  c.concat(b);
	form.submit(); 
}

function getRelation(d) {  
	var data = document.getElementById('query');
	var sql = '';
	
	switch(d) {
		case 1:
		sql = 'SELECT * FROM article LIMIT 20;';
		break;

		case 2:
		sql = 'SELECT * FROM book LIMIT 20;';
		break;

		case 3:
		sql = 'SELECT * FROM www_write LIMIT 20;';
		break;

		case 4:
		sql = 'SELECT * FROM edited_by LIMIT 20;';
		break;

		case 5:
		sql = 'SELECT * FROM publication LIMIT 20;';
		break;

		default:
		break;
	}

	data.value = sql;
	form.submit(); 
}

function getQuery() {  
	var input = document.getElementById('defQuery'); 
	var d = input.value;
	var data = document.getElementById('query');
	var sql = ''; 
	switch(parseInt(d)) {
		case 1:
		sql = "SELECT p.year, p.title FROM publication p WHERE p.dblp_key IN (SELECT dblp_key FROM article_write WHERE author='Philip S. Yu') OR p.dblp_key IN (SELECT dblp_key FROM authorbook_write WHERE author='Philip S. Yu') OR p.dblp_key IN (SELECT dblp_key FROM incollection_write WHERE author='Philip S. Yu') OR p.dblp_key IN (SELECT dblp_key FROM inproceedings_write WHERE author='Philip S. Yu')ORDER BY p.year DESC LIMIT 10;";
		break;

		case 2:
		sql = "SELECT COUNT(*) FROM (select * from publication p, article m where p.dblp_key = m.dblp_key) A, (select * from publication p, inproceedings m where p.dblp_key = m.dblp_key) I WHERE I.title = A.title AND I.year < A.year;";
		break;

		case 3:
		sql = 'SELECT a.author, COUNT(*) FROM article_write AS a, article_write AS b WHERE a.dblp_key = b.dblp_key AND a.rank != b.rank GROUP BY a.author ORDER BY COUNT(*) DESC LIMIT 10;';
		break; 

		default:
		break;
	}

	data.value = sql;
	form.submit(); 
}