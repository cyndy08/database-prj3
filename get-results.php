<?php
    date_default_timezone_set("America/New_York");
	$title = " | Query Results";
    require("./includes/views/results-header.php"); 
?> 

<?php
	include('./includes/process.php');
?>
<contents>
        <div class="row medium-unstacked" id="startOfContents">
          	<div class="medium-12 columns"> 
                    <div class="sections">
                      
                      <section id="queries" data-magellan-target="queries">
                      		<div class="large callout">
                          
                            <h4>Results</h4>
                            <hr />
                             <ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                              <li class="accordion-item" data-accordion-item>
                                  <a href="#" class="accordion-title is-active">Toggle Results</a>
                                  <div class="accordion-content" data-tab-content>
                                  <?php echo getResults(); ?>
                                  </div>
                              </li>
                            </ul> 
                            <div style="text-align: right">
                            	<a href="../~cyndy08/index.php" class="button secondary">Go back</a>
                            </div>
                        	</div>
                      
                      
                      </section>
                      
                      
                      
                    </div> 
            </div> 
        </div>
    </contents>
<?php
    require("./includes/views/footer.php");
?>