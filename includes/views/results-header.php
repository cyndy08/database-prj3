<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CS4604 Project: FDC <?php echo ($title == null ? '' : $title)?></title>
    
    <!-- le css --> 
    <link rel="stylesheet" href="../~cyndy08/includes/css/foundation.css">
    <link rel="stylesheet" href="../css/app.css"> 
    <link rel="stylesheet" href="/~cyndy08/includes/css/foundation.css">
    <link rel="stylesheet" href="/~cyndy08/includes/css/app.css"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    
    <!-- le fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Rajdhani:700' rel='stylesheet' type='text/css'>

  </head>
  <body>  
  
  	<header>
    	<div id="head">
        	<br />
        	<!-- title -->
            <h1 class="noSpaceDown">Biblio<i><b class="vtmaroon">VT</b></i> - FDC</h1> 
            <h4 class="noSpaceDown">computer science bibliography</h4>
            <h5>CS4604: Introduction to Database Management Systems Project</h5>
            <h6>By: <b>F</b>ilip Gouglev, <b>D</b>arius Vallejo, <b>C</b>yndy Marie Ejanda </h6>
        </div>
        <div class="">
        <div data-sticky-container>
          <div class="sticky" id="menu" data-sticky data-margin-top="0" style="width:100%; padding-left: 12em; padding-right: 12em" data-margin-bottom="0" data-top-anchor="startOfContents" data-btm-anchor="startOfContents:bottom">
            <nav data-magellan>
              <ul class="horizontal menu expanded">
              <li><a href="../~cyndy08/index.php"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
              <li><a href="../~cyndy08/index.php#about"><i class="fa fa-book" aria-hidden="true"></i>About</a></li>
              <li><a href="../~cyndy08/index.php#queries"><i class="fa fa-database" aria-hidden="true"></i>User Queries</a></li>
              <li><a href="../~cyndy08/index.php#tools"><i class="fa fa-wrench" aria-hidden="true"></i>Tools</a></li>
              <li><a href="../~cyndy08/index.php#faqs"><i class="fa fa-question-circle-o" aria-hidden="true"></i>FAQs</a></li>
              </ul>
            </nav>
          </div>
        </div>
        </div>
        <div class="row medium-unstacked"><div class="medium-12 columns"><br /></div></div>
    </header>
